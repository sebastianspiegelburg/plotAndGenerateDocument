
import unittest
import os
import io
import sys
from pathlib import Path

from generator.generatePlots import *
from generator.generateHtmlDocument import *
from test.testGenerator import *


from generator.generateApdfDocument import *
from generator.generateHtmlDocument import htmlGenerator
from wheel.signatures import assertTrue


class TestforWholeProject(unittest.TestCase):

    testFileDirectory = './test/htmlMako'
    makoInputFile = 'index.html' 
    outputPathName = "./test/tmp/"
    outputFileName = "testFile.html"
    testContent = "HelloWorld"
    inputFileToCheck = outputFileName[:-5] + "Success.html" 
    
    '''
    Tests for generateApdf
    '''
    
    def test_showsConententofFIle(self):
        myPDF = makePdf()
        myPDF.setInputfilePathName(self.outputPathName)
        myPDF.setInputfileName(self.inputFileToCheck)
        myPDF.setOutputfilePathName(self.outputPathName)
        myPDF.setOutputfileName("test.pdf")

        capturedOutput = io.StringIO()                  # Create StringIO object
        sys.stdout = capturedOutput                     #  and redirect stdout.
        myPDF.showContenthtml()
        
        accessTypeRead = 'r'
        path = myPDF.getInputfilePathName() + myPDF.getInputfileName()
        file = open(path, accessTypeRead)
        text = file.read().strip() 
        text = text + "\n"

        sys.stdout = sys.__stdout__                         # Reset redirect.
        self.assertEqual(capturedOutput.getvalue(), text)

    def test_generationOfPDFformHTML(self):
        myPDF = makePdf()
        myPDF.setInputfilePathName(self.outputPathName)
        myPDF.setInputfileName(self.inputFileToCheck)
        myPDF.setOutputfilePathName(self.outputPathName)
        myPDF.setOutputfileName("test.pdf")
        
        from pathlib import Path 
        output = myPDF.getOutputfilePathName() + myPDF.getOutputfileName()
        my_file = Path(output)
        print(output)
        if my_file.is_file():
            os.remove(output)

        myPDF.generatePdfFromHtml()
        self.assertTrue(my_file.is_file())



    ''' 
    Tests for generatePlot
    '''
    
    def test_checkResultsOfMathematicalFunction(self):
        mathFunctionFrom_cP = customPlot()
        myList = [1,2,3,4,5]
        aValue = 2

        resultList = mathFunctionFrom_cP.makeExpoValueList(myList, aValue)
        correctList = [2,4,8,16,32]
        self.assertEqual(correctList,resultList)
        
        resultNormList = mathFunctionFrom_cP.generateNormedlist(resultList)
        correctNormList = [0.03, 0.06, 0.13, 0.26, 0.52]
        self.assertEqual(correctNormList,resultNormList)

    ''' 
    Tests for  generatingHtml
    '''

    def test_createAndRemoveFile(self):


        fileConnection = htmlGenerator()
        fileConnection.setOutputPathName(self.testFileDirectory)
        fileConnection.setOutputFileName(self.outputFileName)
        file = fileConnection.generateFileConnection(self.testContent)
        file.close()

        my_file = Path(self.testFileDirectory + self.outputFileName)
        isAFile = my_file.is_file()
        self.assertTrue(isAFile)
        
        os.remove(self.testFileDirectory + self.outputFileName)
        my_file = Path(self.testFileDirectory + self.outputFileName)
        isAFile = my_file.is_file()
        self.assertFalse(isAFile)

    def test_createHtmlFileWithTemplateEngine(self): 

        fileConnection = htmlGenerator()
        fileConnection.setBaseDirectory(self.testFileDirectory)
        fileConnection.setMainTemplateFilename(self.makoInputFile)
        fileConnection.initalise()

        fileConnection.setOutputPathName(self.outputPathName)
        fileConnection.setOutputFileName(self.outputFileName)
        fileConnection.generateHtmlFile()
        self.assertTrue(True)

    def test_method_ShowRenderTemplate(self):
        capturedOutput = io.StringIO()                  # Create StringIO object
        sys.stdout = capturedOutput                     #  and redirect stdout.
        testString = "Hello ,James"
        fileConnection = htmlGenerator()
        fileConnection.mytemplate = Template(testString)
        fileConnection.showRenderTemplate()

        sys.stdout = sys.__stdout__                         # Reset redirect.
        # print Statement has \n so we need to add it
        testString = testString + "\n"
        self.assertEqual(capturedOutput.getvalue(), testString)

    def test_generateHtmlFromMakoTemplate(self):

        html1 = htmlGenerator()
        html1.setBaseDirectory(self.testFileDirectory)
        html1.setMainTemplateFilename(self.makoInputFile)
        html1.initalise()

        html1.setOutputPathName(self.outputPathName)
        html1.setOutputFileName(self.outputFileName)
        html1.generateHtmlFile()
        os.path.getsize(self.outputPathName + self.outputFileName)
        renameOutputFile = self.outputFileName[:-5]
        sizeOfOutputFIle = os.path.getsize(self.outputPathName + self.outputFileName)
        sizeOfOutputFileCorrect =os.path.getsize(self.outputPathName + renameOutputFile +"Success.html")
        self.assertEqual(sizeOfOutputFIle, sizeOfOutputFileCorrect)
