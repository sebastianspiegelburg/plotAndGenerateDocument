'''
Created on Apr 26, 2018

@author: Sebastian Spiegelburg
'''
from generator import  generatePlots
from generator import generateHtmlDocument

from test.testGenerator import *
from generator.generatePlots import customPlot
from matplotlib.pyplot import stackplot
from email._header_value_parser import Parameter


if __name__ == '__main__':
    '''
   Uncomment the following line to test the code 
    '''
    #unittest.main()   


    '''
    The following code creates  linare and stacked bar plots  

    '''
    pathToPlacePlot = "./tmp/png/"
    linearePlotLabel = "linPlot"
    stackedBarPlotLabel = "stackPlot" 
    format = ".png"
    parameterAttribute = { "format": format, "linearePlotLabel":linearePlotLabel,"stackedBarPlotLabel" : stackedBarPlotLabel }
    def interval(startValue, step):
        index = 1
        i =  startValue
        while i <= startValue * step:
            yield i 
            index += 1
            i = index * startValue
    def frange(start, stop, step):
        i = start
        while i < stop:
            yield i
            i += step

    sampleSizeX = 8
    index = 1
    for i in frange(0.2,0.9,0.25):
        mathPlotLineareChart = customPlot()
        mathPlotLineareChart.setPathToFolder(pathToPlacePlot)
        mathPlotLineareChart.generatePlotOfNormExpoFunction(sampleSizeX,i,linearePlotLabel + str(index))
        
        mathPlotStackedBar = customPlot()
        mathPlotStackedBar.setPathToFolder(pathToPlacePlot)
        mathPlotStackedBar.generateBarPlot(sampleSizeX, i ,stackedBarPlotLabel + str(index) )
        print(str(index )+ " "+  str(i))
        index += 1

    '''
       The following Code creates the Html FIle from the templated and the given Files.
    '''
    

    parameterAttribute.update({'plotCounter':index})

    inputDirectory = './htmlMako'
    makoInputFile = 'index.html' 
    outputPathName = "./result/"
    outputFileName = "dispalyPlots.html"

    html1 = htmlGenerator()
    html1.setBaseDirectory(inputDirectory)
    html1.setMainTemplateFilename(makoInputFile)
    html1.initalise()
    '''
    The following code creates  linare and stacked bar plots  
    '''

    html1.setOutputPathName(outputPathName)
    html1.setOutputFileName(outputFileName)
    html1.setAttributesDictionary(parameterAttribute)

    html1.showRenderTemplate()
    html1.generateHtmlFile()

    
    ''' 
    The following Code creates the PDF document from the given HTML document
    '''

    myPDF = makePdf()
    myPDF.setInputfilePathName(outputPathName)
    myPDF.setInputfileName(outputFileName)
    myPDF.setOutputfilePathName(outputPathName)
    myPDF.setOutputfileName(outputFileName[:-5] + ".pdf")
    myPDF.generatePdfFromHtml()
	