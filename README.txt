Abstract
========

This project is  an API that lets   user   generate mathematical Plots and lets
them display in a HTML Document and as PDF. See in folder "results" to get  an 
example of a rendered HTML and PDF File. The layout has been kept very basic 
in the html so I could focus on the customer customized  wishes. 

The goal is to create from html directly to pdf. This should save time in 
maintenance and enhancement.


You see from the html what the pdf should look like.


Conclusion
===========

The quality is not sufficient and for further use for my customers not optimal.

In further project I will split the creating process of the web page and the
pdf generation completely for the sake of quality.

Implementation
==============

The Project has been split in multiple parts that get explain in the files 
description. Now follows an overview:

"generatePlots" holds  the instruction to make plots with it holds the 
mathematical formula and can generate plots in certain format by default svg. 
With each object we can create a new plot so it is very easy an quick to create  
many plots at ones.

"generateHtmlDocument" is used to create the html layout with mako template 
engine. Its  main function is to help the template to get organizied. 

"generateAPdfDocument" takes the data from the generated html and makes a 
PDF-Document from it. It service as a clean interface to the library.


--------------------------
Critiques and positive Mentions
--------------------------

+ The project has been test driven development. Most of the written code
  could haven been reused and the "nosetests" Program has been used for testing
  It is more precise than the default "UniteTeste" of Python.

+ In the project design with Version Controller GIT the commits where chosen
  very carefully to display the changes more clearly over the process.

+ A clean and thought-out project structure should make easy to understand 
  the project.

- Deciding to use html to process to an pdf document instate of creating a
  pdf document directly from python had more disadvantages then advantages
        
    Advantages HTML
    --------------------------------------
    + more Standardized & Supported 
    + quick and highly customizable production using mako template engine
    + support of css

    Disadvantage of HTML converting in PDF
    --------------------------------------
    - hardly no support for convertering (FPDF and two other librarys)
    - Poor Qualities of Document 
    - doesn't supported css code

  conclusion writing documents in FPDF directly is more time consuming 
  but have higher quality end result.    
     

- There is more potential in refactoring my currently created code.
 

 
Additional Information to the mathematical formula from "generatePlots"
-------------------------------------------------------------------------
 
As a personal side project I wanted to find a more precise function to
describe the buy-in behaviour of customers. The function should tell how likely 
it is that people buy the same item multiple times. This feature is also 
useful for an other project of mine where i create fictional database to 
simulate data from a sell store.

For example in a small shop like a kiosk  it is typical behaviour for a 
customers to buy different item like chewing gum, chocolate or a soda drink. 
but it is less likely that a customer buys a product multiple times. 

The best function to reflect this behaviour was a norm exponential function.
That you can see under the sample folder "results". Now you can figure out 
what value comes closes to your case an generate your on receipt to simulate
a realistic kiosk.


Please keep in mind that this is a big simplification of a real shop many 
factors that have impact on the realism of my estimation have not been included. 

like promotion, necessity for people, seasons of the year and so on


