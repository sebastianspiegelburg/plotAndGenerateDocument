'''
Created on Apr 26, 2018

@author: Sebastian Spiegelburg

This generates a template for making html document and takes the template and 
creates a html document all given images should get included in the html document.
'''
from mako.template import Template
from mako.lookup import TemplateLookup

class htmlGenerator:


    attributesDictionary = {}
    
    def getAttributesDictionary(self):
        return self.attributesDictionary

    def setAttributesDictionary(self,aDictionary):
        self.attributesDictionary = aDictionary

    def getOutputFileName(self):
        return self.OutputFileName
    def setOutputFileName(self, value):
        self.OutputFileName = value

    def getOutputPathName(self):
        return self.outputPathName
    def setOutputPathName(self, value):
        self.outputPathName = value
    
    def getTemplate(self):
        return self.mytemplate

    def getMainTemplateFilename(self):
        return self.mainTemplateFile
    def setMainTemplateFilename(self,aFileName):
        self.mainTemplateFile = aFileName
        
    def getBaseDirectory(self):
        return self.nameBaseDirectory
    def setBaseDirectory(self,aDirectory):
        self.nameBaseDirectory = aDirectory

    def initalise(self):
        filename = """<%include file=""" + " '" + self.getMainTemplateFilename() + "' "+ """/> """
        mylookup = TemplateLookup(directories=[self.getBaseDirectory()],output_encoding='utf-8')
        self.mytemplate = Template(filename,lookup=mylookup)

    def showRenderTemplate(self):
        print(self.mytemplate.render(attr = self.getAttributesDictionary()))

    

    def generateFileConnection(self,contentsForFIle):
        accessTypeWRITE = 'w'
        path = self.getOutputPathName() + self.getOutputFileName()
        print(path)
        File = open(path, accessTypeWRITE)
        File.write(contentsForFIle)
        return File

    def generateHtmlFile(self):
        File = self.generateFileConnection(self.mytemplate.render(attr = self.getAttributesDictionary()))
        File.close()
