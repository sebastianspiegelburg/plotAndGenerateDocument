'''
Created on Apr 26, 2018

@author: Sebastian Spiegelburg



'''


class customPlot:

    import matplotlib.pyplot as plt
    import math as mt
    
    
    suffixName = ".png"

    def getPathToFolder(self):
        return self.pathToFolder

    def setPathToFolder(self, aPath):
        self.pathToFolder = aPath

    def showPlots(self):
        self.plt.show()

    def generateSumList(self,aList):
        newList = list([])
        listIncreaseVariable = 0
        for iter in aList:
            newList.append(iter + listIncreaseVariable)
            listIncreaseVariable += iter
        return newList
    
    def getPlot(self):
        return self.plt


    def generatePlotOfNormExpoFunction(self,rangeforXValue,expoBase,prefixName):
        xValueList = [ iter  + 1 for iter in range(rangeforXValue)]
        yValueList = self.makeExpoValueList(xValueList,expoBase)
        yValueListNorm = self.generateNormedlist(yValueList)
        nameOfPlot = self.generatePlotLabels(prefixName)
        self.plotLineareChart(xValueList, yValueListNorm, nameOfPlot)

    '''
    Generates exponent Values with percentValue base and the exponent from element in aListX and
    gives a new List with the result of each calculation.
    '''
    def makeExpoValueList(self,aListX,aValue):
        return [self.mt.pow(aValue,iter) for iter in aListX]


    '''
    It takes the sum of all elements from the list and it divides each element through the sum
    return a new list 
    '''
    def generateNormedlist(self,aList,roundFinalResult=2):
        sumOfValueList = sum(aList)
        normList =[ round((iter / sumOfValueList),roundFinalResult) for iter in aList]
        return normList
    '''
    Makes a string lable for the outputing charts.
    '''
    def generatePlotLabels(self,prefixName="plot" ):
        return prefixName   + self.suffixName 

    def generatePlotLabels_old(self,exponentBase, xValueSize,prefixName="plot" ,suffixName=".svg"):
        if not 'plotCounterGlobal' in globals():
            global plotCounterGlobal
            plotCounterGlobal  = 1
        else:
            plotCounterGlobal += 1
        return prefixName + str(plotCounterGlobal) +"expBase" + str(exponentBase) +" posValue" + str(xValueSize) + suffixName

    def plotLineareChart(self,xValueList,yValueList, fileName):
        self.plt.figure(figsize=(10,4))
        self.plt.plot(xValueList, yValueList)
        self.plt.xlabel('case x')
        self.plt.ylabel('likelyhood of case x')
        self.plt.title('Exponential Function')
        self.plt.grid(True)
        self.plt.savefig(self.getPathToFolder() + fileName)




    def generateBarPlot(self,xValueSize,percentValue,prefixName,yLableAxes="",xLableAxes="Distance"):
        xValueList = [ iter + 1 for iter in range(xValueSize)]
        percentages = self.generateNormedlist(self.makeExpoValueList(xValueList,percentValue))
        # makes a list from 0 to i
        y_pos = [0]
        # tells you the scale size of the whole plot (x,y)
        fig = self.plt.figure(figsize=(10,4))
        # generates a grit that i-rows and j-column and we choice the spot 
        ax = fig.add_subplot(111)

#        colors ='rgbmc'
        colors ='rgw'
        patch_handles = []
        left = 0

        for index in range(len(percentages)) :
            colorLetter =colors[ index%len(colors)]
            patch_handles.append(ax.barh(y_pos, percentages[index], 
              color=colorLetter, align='center', 
              #color=colors[3], align='center', 
              left=[left]))
            # accumulate the left-hand offsets
            left += percentages[index]

        for index in range(len(patch_handles)):
            patchChildrean = patch_handles[index].get_children()[0]
            bl = patchChildrean.get_xy()
            x = 0.5*patchChildrean.get_width() + bl[0]
            y = 0.5*patchChildrean.get_height() + bl[1]
            ax.text(x,y, "%d%%" % (100*percentages[index]), ha='center')
            
        ax.set_yticks(y_pos)
        ax.set_yticklabels(yLableAxes)
        ax.set_xlabel(xLableAxes)
        nameOfPlot = self.generatePlotLabels(prefixName)
        self.plt.savefig(self.getPathToFolder() + nameOfPlot)
        self.plt.clf()
       # self.plt.show()