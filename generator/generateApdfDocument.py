'''
Created on Apr 26, 2018

@author: Sebastian Spiegelburg


This  file creates a pdf document in a certain folder that takes an html document 
and all its given images and text.
'''

from fpdf import FPDF, HTMLMixin
 
class MyFPDF(FPDF, HTMLMixin):
    pass

class makePdf:
 

    def generatePdfFromHtml(self):
        
        textContent = self.generateAString()
        print(textContent)
        output = self.getOutputfilePathName() + self.getOutputfileName()

        pdf=MyFPDF()
        pdf.add_page()
        pdf.write_html(textContent)
        pdf.output(output,'F')

    def setOutputfileName(self,aFilename):
        self.OutputFileName = aFilename
    def getOutputfileName(self):
        return  self.OutputFileName

    def setOutputfilePathName(self,anOutputfile):
        self.OutputfilePathName = anOutputfile

    def getOutputfilePathName(self):
        return self.OutputfilePathName

    def setInputfileName(self,anInputFilename):
        self.InputfileName =  anInputFilename
    def getInputfileName(self):
        return self.InputfileName

    def getInputfilePathName(self):
        return self.InputfilePathName

    def setInputfilePathName(self,aFilePathName):
        self.InputfilePathName = aFilePathName

    def generateAString(self):    
        accessTypeRead = 'r'
        path = self.getInputfilePathName() + self.getInputfileName()
        file = open(path, accessTypeRead)
        text = file.read().strip()
        file.close()
        return text

    
    def showContenthtml(self):
        print(self.generateAString())
    

